﻿using UnityEngine;
using System.Collections;

public class ActionManager : MonoBehaviour {
    public GameRules gameRules;
    public void BuildNuke() {
        gameRules.playerCountry.BuildNuke();
    }

    public void MakeWealth() {
        gameRules.playerCountry.MakeWealth();
    }
}

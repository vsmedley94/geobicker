﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Interactions : MonoBehaviour {
    public Button pressure, blackmail, embargo, client, counterinfo;
    public Country country;
    GameRules gameRules;
    public AudioSource audioSource;
    public AudioClip embargoSound;

	// Use this for initialization
	void Start () {
        gameRules = GameObject.Find("GameRules").GetComponent<GameRules>();
        embargo.onClick.AddListener(delegate() {
            if (!gameRules.embargoManager.IsCountryEmbargoedBy(country, gameRules.selectedCountry)) {
                gameRules.embargoManager.SendEmbargo(country, gameRules.selectedCountry);
                audioSource.PlayOneShot(embargoSound);
            } else {
                gameRules.embargoManager.LiftEmbargo(country, gameRules.selectedCountry);
            }
        });

        client.onClick.AddListener(delegate() {
            if (!gameRules.clientManager.IsCountryClientOf(country, gameRules.selectedCountry)) {
                gameRules.clientManager.Clientize(country, gameRules.selectedCountry);
            } else {
                gameRules.clientManager.Release(country, gameRules.selectedCountry);
            }
        });

        pressure.onClick.AddListener(delegate() {
            gameRules.selectedCountry = country;
        });
	}

    void OnEnable() {
        if (gameRules != null) {
            if (gameRules.embargoManager.IsCountryEmbargoedBy(country, gameRules.selectedCountry)) {
                embargo.image.color = Color.red;
            } else {
                embargo.image.color = Color.white;
            }

            if (gameRules.clientManager.IsCountryClientOf(country, gameRules.selectedCountry)) {
                client.image.color = Color.green;
            } else {
                client.image.color = Color.white;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator fadeButtonColor(Button button, Color a, Color b) {
        a = button.image.color;
        for (float i = 0; i <= 1; i += 0.1f) {
            button.image.color = Color.Lerp(a, b, i);
            yield return null;
        }
        button.image.color = b;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MassOrderButton : MonoBehaviour {
    public Color neutralColor, activeColor;
    public Image image;
    public string controlName;
    bool active;

    void Update() {
        if (Input.GetButtonDown(controlName)) {
            ExecuteEvents.Execute<ISubmitHandler>(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
        }
    }

    public void SetActive() {
        active = true;
        SwitchColor();
    }

    public void SetInactive() {
        active = false;
        SwitchColor();
    }

    public void ToggleActive() {
        if (!active) {
            SetActive();
        } else {
            SetInactive();
        }
    }

    void SwitchColor() {
        if (active) {
            image.color = activeColor;
        } else {
            image.color = neutralColor;
        }
    }
}

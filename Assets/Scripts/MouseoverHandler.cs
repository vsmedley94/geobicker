﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MouseoverHandler : MonoBehaviour {
    public Country country;
    public GameRules gameRules;
	// Use this for initialization
	void Start () {
        gameRules = GameObject.Find("GameRules").GetComponent<GameRules>();
	}

    public void Mouseover() {
        gameRules.hoveredCountry = country;
    }

    public void Mouseexit() {
        gameRules.hoveredCountry = gameRules.selectedCountry;
    }
}

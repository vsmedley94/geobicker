﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public enum government {
	democracy,
	dictatorship,
	singleparty,
	theocracy,
}

public enum economy {
	capitalist,
	communist
}
public class Country {
	public static string[] GovernmentNames = new string[] {"Democracy", "Dictatorship", "Single-Party State", "Theocracy"};
	public static string[] EconomyNames = new string[] {"Capitalist", "Communist"};
	public string name, colorName;
	public int gov, econ;
	public Color color;
	public string hexColor;
	public GameObject icon;
	public Vector3 iconTargetPosition;
    public List<Country> clients;
    public List<Country> tradePartners;
    public List<Country> embargoTargets;
    public List<Country> threatTargets, threatsRecieved;
    public Dictionary<Country, float> nukeThreatTimer;
    public Country hegemon;
    public int nukes, instability, wealth;
	
	GameRules gameRules;
    CountryAI AI;
	// Use this for initialization
	public Country () {
		gameRules = GameObject.Find("GameRules").GetComponent<GameRules>();
        Orders.gameRules = gameRules;
		color = new Color(Random.Range(0.2f, 1.0f), Random.Range(0.2f, 1.0f), Random.Range(0.2f, 1.0f));
		hexColor = ColorToHex(color);
		int nameIndex = Random.Range(0, gameRules.countryNames.Length - 1);
		name = "TAKEN";
		while(name == "TAKEN") {
			name = gameRules.countryNames[nameIndex];
			gameRules.countryNames[nameIndex] = "TAKEN";
			nameIndex = Random.Range(0, gameRules.countryNames.Length - 1);
		}
		colorName = "<color=#" + hexColor + ">" + name + "</color>";
		gov = Random.Range(0, 4);
		econ = Random.Range(0, 2);
		gameRules.Log(colorName + " " + EconomyNames[econ] + " " + GovernmentNames[gov]);
		icon = (GameObject)GameObject.Instantiate(Resources.Load("iconPrefab"));
        icon.SetActive(true);
		icon.transform.SetParent(GameObject.Find("IconPanel").transform);
        icon.transform.localPosition = new Vector3(Random.value, Random.value);
		iconTargetPosition = icon.transform.localPosition;
		icon.GetComponentInChildren<Text>().text = colorName;
        icon.GetComponent<Icon>().country = this;
        clients = new List<Country>();
        embargoTargets = new List<Country>();
        tradePartners = new List<Country>();
        threatTargets = new List<Country>();
        threatsRecieved = new List<Country>();
        nukeThreatTimer = new Dictionary<Country, float>();


        AI = new CountryAI(this, gameRules);
	}
	
	// Update is called once per frame
	public void FixedUpdate () {
        AI.FixedUpdate();
        List<Country> threatsToRemove = new List<Country>();
        foreach (Country c in threatsRecieved) {
            if (c.getNukes() >= getNukes() + 5) {
                //cancel nuclear threats against greater powers
                if (threatTargets.Contains(c)) {
                    threatTargets.Remove(c);
                }

                //check if this timer has already started counting
                if (nukeThreatTimer.ContainsKey(c)) {
                    nukeThreatTimer[c]++;
                    if (nukeThreatTimer[c] > 100) {
                        //try to remove a nuke from the stockpile or a client stockpile
                        if (TryDisarm()) {
                            Debug.Log("disarm");
                        } else {
                            //impose will, eventually subjugate
                            if (getNukes() <= 0) {
                                if (this.gov != c.gov) {
                                    instability++;
                                    this.gov = c.gov;
                                } else if (this.econ != c.econ) {
                                    instability++;
                                    this.econ = c.econ;
                                } else if (this.hegemon != c) {
                                    instability++;
                                    gameRules.clientManager.Clientize(this, c);
                                    threatsToRemove.Add(c);
                                }
                            }
                        }
                        nukeThreatTimer[c] = 0f;
                    }
                } else {
                    //establish this timer
                    Debug.Log("maketimer");
                    nukeThreatTimer[c] = 0;
                }
            }
        }

        foreach (Country c in threatsToRemove) {
            gameRules.nukeManager.LiftThreat(c, this);
        }
	}

    public void DeactivateInfoPanel() {
        
    }
	
	public float getOpinion(Country target) {
		int opinion = 0;
		if (target.gov != gov)
			opinion -= 5;
		if (target.econ != econ)
			opinion -= 10;
        if (gameRules.embargoManager.IsEmbargoBetween(this, target)) {
            opinion -= 10;
        }

		return opinion;
	}

    public int getPower() {
        int power = 0;
        power += clients.Count;
        power += nukes;
        foreach (Country client in clients) {
            power += client.getPower();
        }
        return power;
    }

    public int getNukes() {
        int n = 0;
        n += nukes;
        foreach (Country client in clients) {
            n += client.getNukes();
        }
        return nukes;
    }

    public void BuildNuke() {
        if (wealth >= 1) {
            nukes++;
            wealth--;
        }
    }

    public bool TryDisarm() {
        if (nukes > 0) {
            nukes--;
            return true;
        } else {
            foreach (Country c in clients) {
                if (c.TryDisarm()) {
                    return true;
                }
            }
            return false;
        }
    }

    public void MakeWealth() {
        wealth += tradePartners.Count + 1;
    }

    public int GetThreat() {
        return nukes + instability;
    }
	
	string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}
}

public static class Orders
{
    public static GameRules gameRules;
    public static void Pressure(Country sender, Country reciever) {

    }

    public static void TradeAgreement(Country sender, Country reciever) {
        gameRules.tradeManager.SendTradeAgreement(reciever, sender);
    }

    public static void Embargo(Country sender, Country reciever) {
        gameRules.embargoManager.SendEmbargo(sender, reciever);
    }

    public static void MakeClient(Country sender, Country reciever) {
        if (sender.getPower() > reciever.getPower()) {
            gameRules.clientManager.Clientize(reciever, sender);
        }
    }

    public static void NukeThreat(Country sender, Country reciever) {
        if (!sender.threatTargets.Contains(reciever)) {
            gameRules.nukeManager.SendThreat(sender, reciever);
        } else {
            gameRules.nukeManager.LiftThreat(sender, reciever);
        }
    }
}



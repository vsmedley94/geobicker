﻿using UnityEngine;
using System.Collections;

public class RecieverButton : MonoBehaviour {
    public MassOrderManager orderManager;
    public Country country;

    public void ToggleAsReciever() {
        orderManager.ToggleCountryAsReciever(country);
    }
}

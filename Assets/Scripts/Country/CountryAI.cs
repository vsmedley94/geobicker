﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CountryAI {
    float actionTimer = 10;
    float actionInterval = 10;
    GameRules gameRules;
    Country country;
    CountryAIType type;
    public CountryAI(Country country, GameRules gameRules) {
        this.country = country;
        this.gameRules = gameRules;
        type = (CountryAIType)Random.Range(0, 3);
    }
	
	// Update is called once per frame
	public void FixedUpdate () {
        actionTimer -= 1;
        if (actionTimer <= 0) {
            actionTimer = actionInterval;
            Action();
        }
	}

    void Action() {
        switch (type) {
            case CountryAIType.Client:

                break;
            case CountryAIType.Pacifist:

                break;
            case CountryAIType.Peacekeeper:
                EmbargoDisliked();
                NuclearEscalation();
                ForceNuclearDisarm();
                break;
            case CountryAIType.Superpower:
                NuclearArmsRace();
                NuclearSubjugate();
                break;
        }
    }

    void EmbargoDisliked() {
        foreach (Country other in gameRules.countries) {
            if (country.getOpinion(other) < -12) {
                Orders.Embargo(country, other);
            }
        }
    }

    bool NuclearEscalation() {
        if (country.wealth > 0) {
            country.BuildNuke();
            return true;
        } else {
            country.MakeWealth();
            return true;
        }
    }

    bool NuclearArmsRace() {
        foreach (Country other in gameRules.countries) {
            if (other.nukes > country.nukes && other.econ != country.econ) {
                if (country.wealth > 0) {
                    country.BuildNuke();
                    return true;
                } else {
                    country.MakeWealth();
                    return true;
                }
            }
        }
        return false;
    }

    void ForceNuclearDisarm() {
        foreach (Country other in gameRules.countries) {
            if (other.nukes > country.nukes -8 && !country.threatTargets.Contains(other)) {
                Orders.NukeThreat(country, other);
            } else if (country.threatTargets.Contains(other)) {
                if (other.nukes <= 0) {
                    gameRules.nukeManager.LiftThreat(country, other);
                }
            }
        }
    }

    void NuclearSubjugate() {
        foreach (Country other in gameRules.countries) {
            if (other.hegemon != country && country.nukes > other.nukes + 5) {
                Orders.NukeThreat(country, other);
            } else {
                gameRules.nukeManager.LiftThreat(country, other);
            }
        }
    }
}

public enum CountryAIType {
    Superpower,
    Peacekeeper,
    Pacifist,
    Client,
};

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleInfoPanel : MonoBehaviour {
    public GameObject infoPanel, infoText, detailPanel;
	// Use this for initialization
	void Start () {
        detailPanel = GameObject.Find("GameRules").GetComponent<GameRules>().detailPanel;
	}
	
	// Update is called once per frame
	public void TogglePanel () {
        if (infoPanel.activeSelf) {
            DeactivatePanel();
        } else {
            ActivatePanel();
        }
	}

    public void DeactivatePanel() {
        infoPanel.SetActive(false);
        infoText.SetActive(false);
        detailPanel.SetActive(false);
    }

    public void ActivatePanel() {
        foreach (ToggleInfoPanel p in GameObject.FindObjectsOfType<ToggleInfoPanel>()) {
            p.DeactivatePanel();
        }
        infoPanel.SetActive(true);
        infoText.SetActive(true);
        detailPanel.SetActive(true);
    }
}

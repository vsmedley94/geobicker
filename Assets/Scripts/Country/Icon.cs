﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Icon : MonoBehaviour {
    public GameObject infoButton, senderButton, recieverButton, iconImage;
    public StatPanel statPanel;
    public Image statPanelImage;
    public MassOrderManager orderManager;
    public Country country;

    void Start() {
        infoButton.GetComponent<MouseoverHandler>().country = country;
        senderButton.GetComponent<MouseoverHandler>().country = country;
        recieverButton.GetComponent<MouseoverHandler>().country = country;
        statPanel.country = country;
        statPanelImage.color = country.color;
        iconImage.GetComponent<Image>().color = country.color;
        statPanel.gameObject.SetActive(false);
        orderManager = GameObject.Find("Mass Order").GetComponent<MassOrderManager>();
        orderManager.countryIcons.Add(this);
        senderButton.GetComponent<SenderButton>().country = country;
        senderButton.GetComponent<SenderButton>().orderManager = orderManager;
        recieverButton.GetComponent<RecieverButton>().country = country;
        recieverButton.GetComponent<RecieverButton>().orderManager = orderManager;

    }

    public void DeactivateInfoPanel() {
        gameObject.GetComponentInChildren<ToggleInfoPanel>().DeactivatePanel();
    }

    public void ActivateInfoButton() {
        infoButton.SetActive(true);
        senderButton.SetActive(false);
        recieverButton.SetActive(false);
    }

    public void ActivateSenderButton() {
        infoButton.SetActive(false);
        senderButton.SetActive(true);
        recieverButton.SetActive(false);
    }

    public void ActivateRecieverButton() {
        infoButton.SetActive(false);
        senderButton.SetActive(false);
        recieverButton.SetActive(true);
    }
}

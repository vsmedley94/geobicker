﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatPanel : MonoBehaviour {
	Text text;
    public Text detailPanel;
	public Country country;
    public GameRules gameRules;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
        detailPanel = GameObject.Find("DetailPanel").GetComponent<Text>();
        gameRules = GameObject.Find("GameRules").GetComponent<GameRules>();
	}
	
	// Update is called once per frame
	void Update () {
		if (country != null) {
            string str = Country.EconomyNames[country.econ] + "\nnukes: " + country.nukes +"\nopinion of you: " + 
                country.getOpinion(gameRules.playerCountry);
            text.text = str;
            detailPanel.text = country.colorName + "\n" + str + 
                "\nclients: " + country.clients.Count +
                "\nwealth: " + country.wealth +
                "\nnukes: " + country.nukes + 
                "\npower: " + country.getPower() + 
                "\nthreat: " + country.GetThreat();
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class SenderButton : MonoBehaviour {
    public MassOrderManager orderManager;
    public Country country;

    public void ToggleAsSender() {
        orderManager.ToggleCountryAsSender(country);
    }
}

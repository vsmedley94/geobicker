﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GLLineRenderer : MonoBehaviour {
    public Material mat;
    static List<Vector3> points;
    static List<Quad> quads;
    static float thickness = 0.01f;

    void Start() {
        points = new List<Vector3>();
        quads = new List<Quad>();
    }


    public static void DrawWorldLine(Vector3 a, Vector3 b, Color c) {
        a = Camera.main.WorldToScreenPoint(a);
        b = Camera.main.WorldToScreenPoint(b);
        DrawLine(a, b, c);
    }

    public static void DrawLine(Vector3 a, Vector3 b, Color c) {
        a = Camera.main.ScreenToViewportPoint(a);
        a = new Vector3(a.x, a.y, 0);
        b = Camera.main.ScreenToViewportPoint(b);
        b = new Vector3(b.x, b.y, 0);
        Vector3 dir = (b - a);
        dir = new Vector3(dir.x / Screen.height, dir.y / Screen.width, 0);
        dir.Normalize();
        dir = Quaternion.AngleAxis(90, Vector3.forward) * dir;
        float dot = Mathf.Abs(Vector3.Dot(dir, Vector3.left));
        float scaleFactor = Screen.width * (1 - dot) + Screen.height * dot;
        float tempthickness = thickness * (scaleFactor / Screen.width);
        quads.Add(new Quad(a + dir * tempthickness, a - dir * tempthickness, b - dir * tempthickness, b + dir * tempthickness, c));
        
    }

    public void DrawLines() {
        if (this.enabled) {
            GL.PushMatrix();
            mat.SetPass(0);
            GL.LoadOrtho();
            GL.Begin(GL.QUADS);
            GL.Color(Color.red);
            foreach (Quad q in quads) {
                GL.Color(q.color);
                foreach (Vector3 vert in q.verts) {
                    GL.Vertex(vert);
                }
            
            }
            GL.End();
            GL.PopMatrix();
            quads.Clear();
        }
    }
}

struct Quad
{
    public Quad(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Color color) {
        verts = new Vector3[4];
        verts[0] = a;
        verts[1] = b;
        verts[2] = c;
        verts[3] = d;
        this.color = color;
    }

    public Vector3[] verts;
    public Color color;
}

﻿using UnityEngine;
using System.Collections;

public class RenderManager : MonoBehaviour {
    public GLLineRenderer lineRenderer;
    public Camera c;
	// Update is called once per frame
	void Update () {
        GL.Clear(true, true, camera.backgroundColor);
        lineRenderer.DrawLines();
        c.Render();
	}
}

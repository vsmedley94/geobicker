﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClientStateManager : MonoBehaviour {
    public GameRules gameRules;
    public LineRenderer clientLineRenderer;
	// Use this for initialization
	void Start () {
        
	}

    public void Clientize(Country client, Country hegemon) {
        if (!hegemon.clients.Contains(client) && hegemon.hegemon != client) {
            if (client.hegemon != null) {
                client.hegemon.clients.Remove(client);
            }
            client.hegemon = hegemon;
            hegemon.clients.Add(client);
            gameRules.Log(hegemon.colorName + " makes " + client.colorName + " a client state");
        }
    }

    public void Release(Country client, Country hegemon) {
        client.hegemon = null;
        hegemon.clients.Remove(client);
        gameRules.Log(hegemon.colorName + " releases " + client.colorName + " as a client");
    }

    public bool IsCountryClientOf(Country client, Country hegemon) {
        return hegemon.clients.Contains(client);
    }

    public bool IsClientizationBetween(Country a, Country b) {
        Debug.LogError("Not Implemented");
        return false;
    }

    public void DrawClientsOf(Country country) {
        foreach (Country c in country.clients) {
            GLLineRenderer.DrawWorldLine(country.icon.transform.position, c.icon.transform.position, Color.green);
        }
    }
}

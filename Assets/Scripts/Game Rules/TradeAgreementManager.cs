﻿using UnityEngine;
using System.Collections;

public class TradeAgreementManager : MonoBehaviour {
    public GameRules gameRules;

    public void SendTradeAgreement(Country reciever, Country sender) {
        if (!sender.tradePartners.Contains(reciever)) {
            sender.tradePartners.Add(reciever);
            reciever.tradePartners.Add(sender);
            gameRules.Log(sender.colorName + " creates trade agreement with " + reciever.colorName);
        }
    }

    public void CancelTradeAgreement(Country a, Country b) {
        if (a.tradePartners.Contains(b)) {
            a.tradePartners.Remove(b);
            b.tradePartners.Remove(a);
        }
    }

    public void DrawTradeAgreementsOf(Country c) {
        foreach (Country other in c.tradePartners) {
            GLLineRenderer.DrawWorldLine(other.icon.transform.position, c.icon.transform.position, Color.magenta);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class GameRules : MonoBehaviour  {
	Logger logger;
	public List<Country> countries;
	public string[] countryNames;
	GameObject canvas;
	Rect canvasRect;
    public Country playerCountry;
    public Country selectedCountry, hoveredCountry;
    public EmbargoManager embargoManager;
    public TradeAgreementManager tradeManager;
    public ClientStateManager clientManager;
    public NukeThreatManager nukeManager;
    public GameObject selectedCountryHighlighter;
    public IconArrangement iconArrangement;
    public GameObject detailPanel;
	
	void Start() {
        iconArrangement = IconArrangement.Clients;
		countryNames = Resources.Load<TextAsset>("countries").ToString().Split('\n');
		countries = new List<Country>();
		logger = GameObject.Find("EventLog").GetComponent<Logger>();
		canvas = GameObject.Find("IconPanel");
		canvasRect = canvas.GetComponent<RectTransform>().rect;

        for (int i = 0; i < 8; i++) {
            if (playerCountry == null) {
                Country newCountry = new Country();
                playerCountry = newCountry;
                selectedCountry = newCountry;
                hoveredCountry = newCountry;
                countries.Add(newCountry);
            } else {
                countries.Add(new Country());
            }
        }
		
	}
	void FixedUpdate() {
		foreach(Country c in countries) {
            c.FixedUpdate();
        }
        if (iconArrangement == IconArrangement.Opinion) {
            foreach (Country c in countries) {
                foreach (Country other in countries) {
                    Vector3 direction = (other.iconTargetPosition - c.iconTargetPosition).normalized;
                    float distance = (other.iconTargetPosition - c.iconTargetPosition).magnitude;
                    c.iconTargetPosition = c.iconTargetPosition + direction * ((distance + 10 * (c.getOpinion(other) - 22)) / 50);
                }
                float margin = 60f;
                c.iconTargetPosition = new Vector3(Mathf.Clamp(c.iconTargetPosition.x, canvasRect.xMin + margin, canvasRect.xMax - margin), Mathf.Clamp(c.iconTargetPosition.y, canvasRect.yMin + margin, canvasRect.yMax - margin), c.iconTargetPosition.z);
            }
        } else if (iconArrangement == IconArrangement.Clients) {
            Vector2 offset = Vector2.zero;
            foreach (Country c in countries) {
                if (c.hegemon == null) {
                    c.iconTargetPosition = offset;
                    offset = PositionClients(c, offset);
                }
            }
            
        }
	}

    Vector2 PositionClients(Country c, Vector2 offset) {
        c.iconTargetPosition = offset * 100;
        if (c.clients.Count > 0) {
            foreach (Country client in c.clients) {
                Vector2 clientOffset = PositionClients(client, offset - new Vector2(0, 1));
                if (clientOffset.x > offset.x) {
                    offset.x = clientOffset.x;
                }
            }
        } else {
            offset.x++;
        }
        return offset;
    }
	
	void Update() {
		foreach(Country c in countries) {
			c.icon.transform.localPosition = Vector3.MoveTowards(c.icon.transform.localPosition, c.iconTargetPosition, Vector3.Distance(c.icon.transform.localPosition, c.iconTargetPosition) / 2);
		}

        if (selectedCountry != null) {
            selectedCountryHighlighter.SetActive(true);
            selectedCountryHighlighter.transform.position = selectedCountry.icon.transform.position + Vector3.up * 100;
        } else {
            selectedCountryHighlighter.SetActive(false);
        }

        if (hoveredCountry != null) {
            if (iconArrangement == IconArrangement.Opinion) {
                embargoManager.DrawEmbargoesOf(hoveredCountry);
                clientManager.DrawClientsOf(hoveredCountry);
                tradeManager.DrawTradeAgreementsOf(hoveredCountry);
                nukeManager.DrawThreatsOf(hoveredCountry);
            } else if (iconArrangement == IconArrangement.Clients) {
                foreach (Country c in countries) {
                    clientManager.DrawClientsOf(c);
                }
            }
        }
	}
	
	public void Log(string s) {
		logger.Log(s);
	}

    public void SetIconArrangement(int i) {
        iconArrangement = (IconArrangement)i;
        countries[0].iconTargetPosition += new Vector3(0.4f, 0.4f, 0);
    }
}

public enum IconArrangement
{
    Opinion,
    Clients
};

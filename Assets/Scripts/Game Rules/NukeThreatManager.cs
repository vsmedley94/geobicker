﻿using UnityEngine;
using System.Collections;

public class NukeThreatManager : MonoBehaviour {
    public GameRules gameRules;

    public void SendThreat(Country sender, Country reciever) {
        sender.threatTargets.Add(reciever);
        reciever.threatsRecieved.Add(sender);
        if (reciever.threatTargets.Contains(sender)) {
            gameRules.Log(sender.colorName + " Enters nuclear standoff with " + reciever.colorName);
        } else {
            gameRules.Log(sender.colorName + " Initiates nuclear crisis against " + reciever.colorName);
        }
    }

    public void LiftThreat(Country sender, Country reciever) {
        if (sender.threatTargets.Contains(reciever)) {
            
            sender.threatTargets.Remove(reciever);
            reciever.threatsRecieved.Remove(sender);
            gameRules.Log(sender.colorName + " Lifts nuclear threat against " + reciever.colorName);
        }
    }

    public void DrawThreatsOf(Country c) {
        foreach (Country other in c.threatsRecieved) {
            GLLineRenderer.DrawWorldLine(other.icon.transform.position, c.icon.transform.position, Color.grey);
            float f;
            if (c.nukeThreatTimer.TryGetValue(other, out f)) {
                GLLineRenderer.DrawWorldLine(other.icon.transform.position, Vector3.Lerp(other.icon.transform.position, c.icon.transform.position, f / 100), Color.white);
            }
        }
        foreach (Country other in c.threatTargets) {
            GLLineRenderer.DrawWorldLine(other.icon.transform.position, c.icon.transform.position, Color.grey);
            float f;
            if (other.nukeThreatTimer.TryGetValue(c, out f)) {
                GLLineRenderer.DrawWorldLine(c.icon.transform.position, Vector3.Lerp(c.icon.transform.position, other.icon.transform.position, f / 100), Color.white);
            }
        }
    }
}

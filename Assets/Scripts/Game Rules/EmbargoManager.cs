﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EmbargoManager : MonoBehaviour {
    public GameRules gameRules;

    void Start() {
    }
    public void SendEmbargo(Country subject, Country imposer) {
        if (!IsCountryEmbargoedBy(subject, imposer)) {
            imposer.embargoTargets.Add(subject);
            gameRules.Log(imposer.colorName + " imposes embargo on " + subject.colorName);
        }
    }

    public List<Country> GetEmbargoSendersTo(Country subject) {
        List<Country> cs = new List<Country>();
        foreach (Country c in gameRules.countries) {
            if (IsCountryEmbargoedBy(subject, c)) {
                cs.Add(c);
            }
        }
        return cs;
    }

    public bool IsCountryEmbargoedBy(Country subject, Country imposer) {
        return imposer.embargoTargets.Contains(subject);
    }

    public bool IsEmbargoBetween(Country a, Country b) {
        return a.embargoTargets.Contains(b) || b.embargoTargets.Contains(a);
    }

    public void LiftEmbargo(Country subject, Country imposer) {
        if (imposer.embargoTargets.Contains(subject)) {
            imposer.embargoTargets.Remove(subject);
        }
    }

    public void DrawEmbargoesOf(Country country) {
        foreach (Country c in country.embargoTargets) {
            GLLineRenderer.DrawWorldLine(country.icon.transform.position, c.icon.transform.position, Color.red);
        }

        foreach (Country c in GetEmbargoSendersTo(country)) {
            GLLineRenderer.DrawWorldLine(country.icon.transform.position, c.icon.transform.position, Color.red);
        }
    }
}

public class Embargo
{
    public Country subject, imposer;
    public Embargo(Country subject, Country imposer) {
        this.subject = subject;
        this.imposer = imposer;
    }
}
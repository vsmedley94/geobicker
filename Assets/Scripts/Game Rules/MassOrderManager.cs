﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class MassOrderManager : MonoBehaviour {
    Country sender, reciever;
    public List<Icon> countryIcons;
    string selectedOrderName;
    bool selectingSender = false, selectingReciever = false;

    public void Start() {
        
    }

    void Update() {
        if (sender != null) {
            GLLineRenderer.DrawLine(Camera.main.WorldToScreenPoint(sender.icon.transform.position), Input.mousePosition, Color.white);
        }
    }

    public void ToggleCountryAsSender(Country c) {
        sender = c;
        foreach (Icon i in countryIcons) {
            i.ActivateRecieverButton();
        }
    }

    public void ToggleCountryAsReciever(Country c) {
        reciever = c;
        ExecuteOrders();
        sender = null;
        reciever = null;
        foreach (Icon i in countryIcons) {
            i.ActivateInfoButton();
        }
    }

    public void SelectOrder(string name) {
        selectedOrderName = name;
        foreach(Icon i in countryIcons) {
            i.ActivateSenderButton();
        }
    }

    public void SelectRecievers() {
        
    }

    public void ExecuteOrders() {
            switch (selectedOrderName) {
                case "pressure":
                    Orders.Pressure(sender, reciever);
                    break;
                case "tradeagreement":
                    Orders.TradeAgreement(sender, reciever);
                    break;
                case "embargo":
                    Orders.Embargo(sender, reciever);
                    break;
                case "client":
                    Orders.MakeClient(sender, reciever);
                    break;
                case "nukethreat":
                    Orders.NukeThreat(sender, reciever);
                    break;
            }
        
    }
}

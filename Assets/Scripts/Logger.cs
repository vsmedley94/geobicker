﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Logger : MonoBehaviour {
	public Text text;
	string textToLog;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	float charTimer;
	bool tempTagInserted;
	string nextTag;
	void Update () {
		charTimer--;
		if (charTimer <= 0 ) {
			charTimer = 0;
            for (int i = 0; i < 3; i++) {
                PrintChar();
            }
		}
	}

    void PrintChar() {
        if (textToLog != null && textToLog.Length > 0) {
            if (tempTagInserted) {
                text.text = text.text.Remove(text.text.Length - nextTag.Length, nextTag.Length);
                tempTagInserted = false;
            }
            if (textToLog[0] == '<') {
                while (textToLog[0] != '>') {
                    text.text += textToLog[0];
                    textToLog = textToLog.Substring(1);
                }
                text.text += textToLog[0];
                textToLog = textToLog.Substring(1);

            } else {
                if (textToLog.Length > 0) {
                    text.text += textToLog[0];
                    textToLog = textToLog.Substring(1);
                }
            }
            if (textToLog.Length > 0) {
                nextTag = getNextTag(textToLog);
                if (nextTag != "" && nextTag.Substring(0, 2) == "</") {
                    text.text += nextTag;
                    tempTagInserted = true;
                }
            }
        }
    }
	
	string getNextTag(string s) {
		
		while(s[0] != '<') {
			if (s.Length > 1) {
				s = s.Substring(1);
			} else {
				return "";	
			}
		}
		string tag = "";
		while(s[0] != '>') {
			tag += s[0];
			s = s.Substring(1);
		}
		return tag + '>';
	}
	
	public void Log(string s) {
		
		textToLog += s + "\n";
	}
}
